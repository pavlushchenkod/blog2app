from django.conf.urls import url
from blog2app import views

from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^page(?P<page>\d+)/$', views.HomePageView.as_view(), name='home_page'),
    url(r'^category/(?P<category>[A-Za-z0-9]+)/$', views.HomePageView.as_view(), name='home_page'),
    url(r'^article/(?P<pk>\d+)/$', views.ArticleView.as_view(), name='article'),
    url(r'^article/add_comment/(?P<article_id>[0-9]+)/$', views.CommentCreate.as_view(), name='comment_create'),
    url(r'^add_like/(?P<article_id>[0-9]+)/$', views.AddLikeView.as_view(), name='add_like'),
    url(r'^registration$', views.RegistrationView.as_view(), name='registration'),
    url(r'^login$', auth_views.login, {'template_name': 'blog2app/login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
]
