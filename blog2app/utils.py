

def get_list_pages(current_page, number_all_pages):
    current_page = int(current_page)
    pages_per_line = 3  # unpaired number!
    list_pages = []

    if number_all_pages <= pages_per_line:
        for number in range(1, number_all_pages + 1):
            list_pages.append(number)

    elif current_page <= pages_per_line // 2:
        for number in range(1, (pages_per_line + 1)):
            list_pages.append(number)

    elif current_page >= (
            number_all_pages - (pages_per_line // 2 - 1)):
        for number in range(
                (number_all_pages - (pages_per_line - 1)),
                (number_all_pages + 1)):
            list_pages.append(number)
    else:
        for number in range(current_page - (pages_per_line // 2),
                            current_page + (
                                    pages_per_line // 2 + 1)):
            list_pages.append(number)
    return list_pages


if __name__ == '__main__':
    current_list_page = get_list_pages(current_page=11,
                                   number_all_pages=26)
    print(current_list_page)