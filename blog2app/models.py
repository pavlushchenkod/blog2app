from django.db import models
from django.contrib.auth.models import User


class UserInformation(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100, default=None)
    last_name = models.CharField(max_length=100, default=None)
    birthday = models.DateField(default=None)
    city = models.CharField(max_length=30, default=None)

    def __str__(self):
        return self.user.name


class Category(models.Model):
    name = models.CharField(max_length=7)
    description = models.CharField(max_length=30, null=True)
    image = models.ImageField(upload_to='categories',
                              default='categories/default_category.jpg')

    def __str__(self):
        return self.name


class Article(models.Model):
    name = models.CharField(max_length=100)
    category = models.ManyToManyField(Category, db_table='ManyToMany_Articles_vs_Category')
    description = models.CharField(max_length=200)
    body_text = models.TextField()
    pub_date = models.DateField(auto_now_add=True)
    edit_date = models.DateField(auto_now=True)
    quantity_comments = models.IntegerField(default=0)
    quantity_likes = models.IntegerField(default=0)
    image = models.ImageField(upload_to='articles', default='articles/default_article.jpg')

    def __str__(self):
        return self.name


class Comment(models.Model):
    text = models.TextField()
    pub_datetime = models.DateTimeField(auto_now_add=True)
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.text


class Like(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
