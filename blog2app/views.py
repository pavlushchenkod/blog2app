from django.views.generic import ListView, DetailView, View
from django.views.generic.edit import CreateView, FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.urls import reverse_lazy

from blog2app.models import Article, Comment, Like, Category
from blog2app.forms import RegistrationForm
from blog2app.utils import get_list_pages


class HomePageView(ListView):
    model = Article
    template_name = 'blog2app/main.html'
    context_object_name = 'articles'
    paginate_by = 3

    def get_queryset(self):
        if 'category' in self.kwargs:
            queryset = Article.objects.filter(
                category__name=self.kwargs['category']
            ).order_by('-pub_date')
        else:
            queryset = Article.objects.all().order_by('-pub_date')
        return queryset

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data()

        if 'page' in self.request.GET:
            context['list_pages'] = get_list_pages(
                current_page=self.request.GET.get('page'),
                number_all_pages=int(
                    round(self.object_list.count()/self.paginate_by)+0.5
                ))
        context['categories'] = Category.objects.all()
        return context


class ArticleView(DetailView):
    model = Article
    context_object_name = 'article'
    template_name = 'blog2app/article.html'

    def get_context_data(self, object):
        context = super(ArticleView, self).get_context_data()
        context['comments'] = Comment.objects.filter(
            article=self.object
        ).order_by('-pub_datetime')
        return context


class AddLikeView(LoginRequiredMixin, View):
    login_url = '/blog2app/login'

    @staticmethod
    def get(request, article_id):
        # перевірка наявності лайка від юзера
        is_like = Like.objects.filter(article_id=article_id,
                                      user_id=request.user.id).exists()
        # додаємо або видаляємо лайк
        if not is_like:
            new_like = Like(
                article_id=article_id,
                user_id=request.user.id)
            new_like.save()
        else:
            Like.objects.filter(article_id=article_id,
                                user_id=request.user.id).delete()
        # рахуємо к-ть лайків для статті
        quantity_likes = Like.objects.filter(article_id=article_id).count()
        # оновлюємо к-ть лайків для статті
        Article.objects.filter(id=article_id).update(
            quantity_likes=quantity_likes
        )
        # повернення на поточну сторінку
        return redirect(request.GET['next'])


class CommentCreate(LoginRequiredMixin, CreateView):
    login_url = '/blog2app/login'
    model = Comment
    fields = ['text']
    template_name = 'blog2app/article.html'
    success_url = reverse_lazy('home_page')

    def form_valid(self, form):
        form.instance.user_id = self.request.user.id
        form.instance.article_id = self.kwargs['article_id']
        super().form_valid(form)

        current_article = Article.objects.get(id=self.kwargs['article_id'])
        current_article.quantity_comments = Comment.objects.filter(
            article_id=self.kwargs['article_id']
        ).count()
        current_article.save()
        return redirect(self.request.GET['next'])


class RegistrationView(FormView):
    template_name = 'blog2app/registration.html'
    form_class = RegistrationForm
    success_url = '/blog2app/login'

    def form_valid(self, form):
        is_username = User.objects.filter(
            username=form.cleaned_data['username']
        ).exists()
        if not is_username:
            user = User.objects.create_user(username=form.cleaned_data['username'],
                                            password=form.cleaned_data['password'],
                                            email=form.cleaned_data['email'])
            user.save()
            return super().form_valid(form)
        else:
            return redirect('/blog2app/registration')